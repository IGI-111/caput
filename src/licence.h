#ifndef LICENCE_HEADERFILE
#define LICENCE_HEADERFILE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define MAX_LICENCE_TEXT_LENGTH 1000

void makeLicenceText(char *licence, char licenceText[MAX_LICENCE_TEXT_LENGTH]);

#endif
