#include "licence.h"

void makeLicenceText(char *licence, char licenceText[MAX_LICENCE_TEXT_LENGTH])
{
  char *licenceName = NULL;
  char *licenceAdress = NULL;
  char *licenceAuthor = NULL;
  char *licenceLastVersion = NULL;

  if(licence == NULL)
  {
    licenceText[0] = '\0';
    return;
  }
  else if(strcmp(licence, "GPL") == 0)
  {
    licenceName = "GNU General Public License";
    licenceAdress = "http://www.gnu.org/licenses/";
    licenceAuthor = "the Free Software Foundation";
    licenceLastVersion = "3";
  }
  else if(strcmp(licence, "LGPL") == 0)
  {
    licenceName = "Lesser GNU General Public License";
    licenceAdress = "http://www.gnu.org/licenses/";
    licenceAuthor = "the Free Software Foundation";
    licenceLastVersion = "2.1";
  }
  else if(strcmp(licence, "AGPL") == 0)
  {
    licenceName = "Affero GNU General Public License";
    licenceAdress = "http://www.gnu.org/licenses/";
    licenceAuthor = "the Free Software Foundation";
    licenceLastVersion = "3";
  }
  else if(strcmp(licence, "WTFPL") == 0)
  {
    licenceName = "Do What The Fuck You Want To Public License";
    licenceAdress = "http://www.wtfpl.net/copying/";
    licenceAuthor = "Sam Hocevar";
    licenceLastVersion = "2";
  }
  else
  {
    licenceText[0] = '\0';
    return;
  }
  sprintf(licenceText,
      "\n"
      "This program is free software: you can redistribute it and/or modify\n"
      "it under the terms of the %s as published by\n"
      "%s, either version %s of the License, or\n"
      "(at your option) any later version.\n"
      "\n"
      "This program is distributed in the hope that it will be useful,\n"
      "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
      "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
      "%s for more details.\n"
      "\n"
      "You should have received a copy of the %s\n"
      "along with this program. If not, see <%s>.\n", licenceName, licenceAuthor, licenceLastVersion, licenceName, licenceName,licenceAdress);
}

