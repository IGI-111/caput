#include "comment.h"

void makeCommentSymbols(char *filename, char *begin, char *end)
{
  /*do the tests on the filename here*/
  char *fileExtension = strrchr(filename,'.');
  if(fileExtension == NULL)
  {
    /*plaintext*/
    begin = NULL;
    end = NULL;
    return;
  }
  else if(strcmp(fileExtension,".c") == 0)
  {
    /*C*/
    begin = "/*";
    end = "*/";
    return;
  }
  else if(strcmp(fileExtension,".lua") == 0)
  {
    /*lua*/
    begin = "--";
    end = NULL;
    return;
  }
  else
  {
    /*unknown*/
    begin = NULL;
    end = NULL;
    return;
  }
}
