#ifndef MAIN_HEADERFILE
#define MAIN_HEADERFILE

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>

#include "FIGlet.h"
#include "licence.h"
#include "comment.h"

void displayAndWriteHeader(char *fileName, char *licence, char *author, int plaintext, char* font);
void printUsage(void);
void printHeader(FILE *stream, char *fileName, char *licence, char *year, char *author, char *licenceText, int plaintext, char* font);

#endif
