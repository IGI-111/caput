#include "FIGlet.h"

void FIGletPrint(char *text, FILE *stream, char *font)
{
  const char *FIGletBaseCommand = "figlet -lkn -w MAX_FIGLET_LINE_LENGTH ";
  char *FIGletCommand;
  if(font == NULL)
  {
    FIGletCommand = malloc(strlen(FIGletBaseCommand) + strlen(text) + 1);
    memcpy(FIGletCommand, FIGletBaseCommand, strlen(FIGletBaseCommand));
    memcpy(FIGletCommand+strlen(FIGletBaseCommand), text, strlen(text)+1);
  }
  else
  {
    FIGletCommand = malloc(strlen(FIGletBaseCommand) + 3 + strlen(font) + 1 + strlen(text) + 1);
    memcpy(FIGletCommand, FIGletBaseCommand, strlen(FIGletBaseCommand));
    memcpy(FIGletCommand+strlen(FIGletBaseCommand), "-f ", 3);
    memcpy(FIGletCommand+strlen(FIGletBaseCommand) + 3, font, strlen(font));
    memcpy(FIGletCommand+strlen(FIGletBaseCommand) + 3 + strlen(font), " ", 1);
    memcpy(FIGletCommand+strlen(FIGletBaseCommand) + 3 + strlen(font) + 1, text, strlen(text) + 1);
  }
  char FIGletOutput[MAX_FIGLET_LINE_LENGTH];
  FILE *FIGletStream =
#ifdef _WIN32
    _popen(FIGletCommand,"r");
#else
  popen(FIGletCommand,"r");
#endif
  if(FIGletStream == NULL)
  {
    printf("FIGlet could not be executed\n");
    abort();
  }
  while(fgets(FIGletOutput,sizeof(FIGletOutput),FIGletStream) != NULL)
    fputs(FIGletOutput, stream);
#ifdef _WIN32
  _pclose(FIGletStream);
#else
  pclose(FIGletStream);
#endif
  free(FIGletCommand);
}

