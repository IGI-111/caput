#ifndef FIGLET_HEADERFILE
#define FIGLET_HEADERFILE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_FIGLET_LINE_LENGTH 80

void FIGletPrint(char *text, FILE *stream, char *font);

#endif
