#ifndef COMMENT_HEADERFILE
#define COMMENT_HEADERFILE

#include <stdlib.h>
#include <string.h>

void makeCommentSymbols(char *filename, char *begin, char *end);

#endif
