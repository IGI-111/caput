#include "main.h"

/*TODO: make comment symbols work...again*/

int main(int argc, char *argv[])
{
  char *licence = NULL;
  char *outputFileName = NULL;
  char *author = NULL;
  int plaintext = 0;
  char *font = NULL;

  int opt;
  while((opt = getopt(argc, argv, "l:a:hpf:")) != EOF)
    switch(opt)
    {
      case 'l':
        licence = optarg;
        break;
      case 'a':
        author = optarg;
        break;
      case 'p':
        plaintext = 1;
        break;
      case 'f':
        font = optarg;
        break;
      case '?':
        printUsage();
        return EXIT_FAILURE;
      case 'h':
        printUsage();
        return EXIT_FAILURE;
        /*default:
          return EXIT_FAILURE;*/
    }
  if(optind >= argc)
    displayAndWriteHeader(outputFileName, licence, author, plaintext, font);
  else
    for(opt = optind;opt < argc;opt++)
    {
      outputFileName = argv[opt];
      displayAndWriteHeader(outputFileName, licence, author, plaintext, font);
    }
  return EXIT_SUCCESS;
}

void displayAndWriteHeader(char *fileName, char *licence, char *author, int plaintext, char* font)
{
  /*setting the current year*/
  char year[5];
  time_t now;
  time(&now);
  strftime(year,5,"%Y",localtime(&now));

  /*making the licence text*/
  char licenceText[MAX_LICENCE_TEXT_LENGTH];
  makeLicenceText(licence, licenceText);

  /*making the comment symbols*/
  char *beginComment = NULL;
  char *endComment = NULL;
  makeCommentSymbols(fileName, beginComment, endComment);

  /*printing the header*/
  /*in stdout*/
  printHeader(stdout, fileName, licence, year, author, licenceText, plaintext, font);

  /*in the file*/
  if(fileName != NULL)
  {
    FILE *fileStream = fopen(fileName,"r+");
    if(fileStream == NULL)
    {
      /*file is not found, we just create it and append*/
      fileStream = fopen(fileName,"w");
      printHeader(fileStream, fileName, licence, year, author, licenceText, plaintext, font);
    }
    else
    {
      /*file was found, we have to pull it to memory to prepend the header*/
      fseek(fileStream, 0, SEEK_END);
      size_t fileSize = ftell(fileStream);
      rewind(fileStream);
      char *buffer = malloc(fileSize);
      char c = fgetc(fileStream);
      int i;
      for(i = 0;c != EOF;i++)
      {
        buffer[i] = c;
        c = fgetc(fileStream);
      }
      /*now we write the header*/
      rewind(fileStream);
      printHeader(fileStream, fileName, licence, year, author, licenceText, plaintext, font);
      /*and we write the file at the end of the header*/
      fputs(buffer, fileStream);
      free(buffer);
    }

    fclose(fileStream);
  }
}

void printHeader(FILE *stream, char *fileName, char *licence, char *year, char *author, char *licenceText, int plaintext, char* font)
{
  if(plaintext)
    fprintf(stream, "%s\n", fileName);
  else
    FIGletPrint(fileName,stream, font);
  if(licence != NULL)
  {
    fprintf(stream, "Copyright (C) %s", year);
    if(author != NULL)
      fprintf(stream, " %s\n", author);
    if(licenceText != NULL)
      fprintf(stream, " %s", licenceText);
  }

}



void printUsage(void)
{
  printf(
      "the Caput header generator\n"
      "USAGE: caput [options] <files>\n"
      "OPTIONS:\n"
      "  -a <author name>    Appends the author's name to the header's copyright\n"
      "  -l <licence name>   Adds the specified licence prefix to your header\n"
      "  -f <FIGLet font>    Sets the font to the specified FIGLet font\n"
      "  -p                  Plaintext only\n"
      "  -h                  Displays this list of options\n");
}



