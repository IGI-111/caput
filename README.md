Caput
=====

File header generator

Usage:
<code>caput [-l LICENCE] [-a AUTHOR] [-h] [FILENAME]</code>
