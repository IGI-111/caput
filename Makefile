##################################################

# Project name
TARGET   = caput

# Compiler and flags
CC       = gcc
CFLAGS   = -Wall -g

# Linker and flags
LINKER   = gcc -o
LFLAGS   = -Wall -g

# Project directories
SRCDIR   = src
OBJDIR   = obj
BINDIR   = bin
LIBDIR	 = lib

##################################################

SOURCES  := $(wildcard $(SRCDIR)/*.c)
INCLUDES := $(wildcard $(SRCDIR)/*.h)
LIBS  	 := $(wildcard $(LIBDIR)/*.a)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
rm       = rm -f

$(BINDIR)/$(TARGET): $(OBJECTS)
	@$(LINKER) $@ $(LFLAGS) $(OBJECTS) $(LIBS)
	@echo "Linking complete!"

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@$(CC) $(CFLAGS) -c $< -o $@ #$(LIBS)
	@echo "Compiled "$<" successfully!"

.PHONEY: clean
clean: remove
	@$(rm) $(OBJECTS)
	@echo "Cleanup complete!"

.PHONEY: remove
remove:
	@$(rm) $(BINDIR)/$(TARGET)
	@echo "Executable removed!"

install: $(BINDIR)/$(TARGET)
	@install -s $(BINDIR)/$(TARGET) /usr/bin
	@echo "Install complete!"
